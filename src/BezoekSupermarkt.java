import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BezoekSupermarkt {
    Scanner scanner;
    List<Afdeling> afdelingen = new ArrayList<>();
    Kassa kassa = new Kassa();

    public BezoekSupermarkt(Scanner invoer) {
        this.scanner = invoer;
        this.afdelingen.add(new Bakkerij(1));
        this.afdelingen.add(new Agf(2));
        this.afdelingen.add(new Houdbaar(3));
        this.afdelingen.add(new Verswaren(4));
    }

    void bezoeken() throws Exception {

        System.out.println("\n\nWelkom in supermarkt " + Supermarkt.naam);

        System.out.println("Welke afdeling wilt u bezoeken? Typ juiste ID in.");
        for (Afdeling a : afdelingen) {
            System.out.println(String.format("ID: %s = afdeling %s", a.getId(), a.getAfdelingsnaam()));
        }

        String invoer = this.scanner.nextLine();

        if (Integer.parseInt(invoer) > this.afdelingen.size()) {
            System.err.println("INVOER ONGELDIG!!! Probeer nieuwe invoer.");
            bezoeken();
        }

        Afdeling afdeling = afdelingen.get(Integer.parseInt(invoer) - 1);
        System.out.println("Gekozen afdeling " + afdeling.getAfdelingsnaam());

        System.out.println("Typ 'v' om voorraad te checken");
        System.out.println("OF typ 'tv' voor totaalvoorraad van de supermarkt");
        System.out.println("Typ 'add' en typ vervolgens het aantal om voorraad te verhogen");
        System.out.println("Typ 'koop' en typ vervolgens het aantal om te kopen");
        System.out.println("Typ 'q' om de applicatie te beeindigen");

        invoer = this.scanner.nextLine();

        if (invoer.equals("v")) {
            System.out.println("Voorraad = " + afdeling.getVoorraad());
            bezoeken();
        }
        if (invoer.equals("tv")) {
            System.out.println("Totaalvoorraad supermarkt " + Supermarkt.producten.size());
            for(Product p : Supermarkt.producten){
                System.out.println("Product in vooraad = " + p.naam);
            }
            bezoeken();
        }
        if (invoer.equals("add")) {
            String invoerGetal = this.scanner.nextLine();
            for (int i = 0; i < Integer.parseInt(invoerGetal); i++) {
                afdeling.addProduct((new Product(afdeling.getProductnaam(), afdeling.getPrijs())));
            }
            System.out.println(String.format("Nieuw voorraad = %s, voor afdeling %s.", afdeling.getVoorraad(), afdeling.getAfdelingsnaam()));
            System.out.println("Totaalvoorraad Supermarkt: " + Supermarkt.producten.size());
            bezoeken();
        }
        if (invoer.equals("koop")) {
            String invoerGetal = this.scanner.nextLine();
            if (afdeling.getVoorraad() >= Integer.parseInt(invoerGetal)) {
                for (int i = 0; i < Integer.parseInt(invoerGetal); i++) {
                    try {
                        afdeling.koopProduct();
                    } catch (Exception e) {
                        System.out.println("Product kon niet worden gekocht");
                        throw new Exception(e);
                    }
                }
                System.out.println(String.format("Nieuw voorraad = %s, voor afdeling %s.", afdeling.getVoorraad(), afdeling.getAfdelingsnaam()));
                kassa.omzet += (afdeling.getPrijs() * Integer.parseInt(invoerGetal));
                System.out.println("Totaalvoorraad Supermarkt: " + afdeling.producten.size());
                System.out.println("Totaalomzet Supermarkt: " + kassa.omzet + " euro");
                bezoeken();
            } else {
                System.out.println(String.format("Je wilde %s kopen, maar voorraad is %s. Probeer opnieuw.", invoerGetal, afdeling.getVoorraad()));
                bezoeken();
            }
        }
        if (invoer.equals('q')) {
            System.out.println("Eind applicatie");
        }
    }
}
