import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) throws Exception {
        BezoekSupermarkt bezoekSupermarkt = new BezoekSupermarkt(input);
        bezoekSupermarkt.bezoeken();
    }
}