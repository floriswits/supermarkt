public class Agf extends Afdeling {
    static final String naam = "Agf";

    static final String productnaam = "AGF";
    private int prijsAgf = 4;

    private Product p = new Product(this.productnaam, this.prijsAgf);

    public Agf(int id) {
        super(id);
        super.setAfdelingsnaam(naam);
        super.setPrijs(prijsAgf);
        super.setProductnaam(productnaam);
        super.setProduct(p);
        super.addProduct(p);
    }
}
