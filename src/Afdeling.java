import java.time.Period;

public class Afdeling extends Supermarkt implements Voorraadbeheer {
    private String afdelingsnaam;
    private int id;
    private int prijs;
    private String productnaam;
    private Product product;
    private int voorraad = 0;

    public Afdeling(int id) {
        this.id = id;
    }

    public int getVoorraad() {
        return this.voorraad;
    }

    public void addProduct(Product product) {
        this.voorraad += 1;
        producten.add(product);
    }

    void setProduct(Product p) {
        this.product = p;
    }

    public Product getProduct() {
        return this.product;
    }



    public String getAfdelingsnaam() {
        return this.afdelingsnaam;
    }

    void setAfdelingsnaam(String afdelingsnaam) {
        this.afdelingsnaam = afdelingsnaam;
    }

    public void koopProduct() {
        this.voorraad -= 1;
        for (Product p : producten) {
            if (p.naam.equals(this.productnaam)){
                producten.remove(p);
                break;
            }
        }
    }

    void setPrijs(int prijs) {
        this.prijs = prijs;
    }

    public int getPrijs() {
        return this.prijs;
    }

    public int getId() {
        return this.id;
    }

    void setProductnaam(String productnaam) {
        this.productnaam = productnaam;
    }

    public String getProductnaam() {
        return this.productnaam;
    }


}