public interface Voorraadbeheer {

    int getVoorraad();

    void addProduct(Product product);

    void koopProduct();
}
