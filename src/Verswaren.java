public class Verswaren extends Afdeling implements Apparaat {
    static final String naam = "Verswaren";

    static final String productnaam = "Verswaren";
    private int prijsBoterhambeleg = 2;
    private Product p = new Product(this.productnaam, this.prijsBoterhambeleg);

    public Verswaren(int id) {
        super(id);
        super.setAfdelingsnaam(naam);
        super.setPrijs(prijsBoterhambeleg);
        super.setProductnaam(productnaam);
        super.setProduct(p);
        super.addProduct(p);
    }
}
