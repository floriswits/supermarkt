public class Bakkerij extends Afdeling implements Apparaat{
    static final String naam = "Bakkerij";
    private int prijsBrood = 3;
    private String productnaam = "Brood";
    private Product p = new Product(this.productnaam, this.prijsBrood);


    public Bakkerij(int id) {
        super(id);
        super.setAfdelingsnaam(naam);
        super.setPrijs(prijsBrood);
        super.setProductnaam(productnaam);
        super.setProduct(p);
        super.addProduct(p);
    }
}
