public class Houdbaar extends Afdeling {
    static final String naam = "Houdbaar";

    static final String productnaam = "Cola";
    private int prijsCola = 6;

    private Product p = new Product(this.productnaam, this.prijsCola);

    public Houdbaar(int id) {
        super(id);
        super.setAfdelingsnaam(naam);
        super.setPrijs(prijsCola);
        super.setProductnaam(productnaam);
        super.setProduct(p);
        super.addProduct(p);
    }
}
